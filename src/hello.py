def do_add(x,y):
    return x + y

def do_sub(x, y):
    return x - y

def handle_request(request, response, timer=None):
    x = int(request.get('x'))
    y = int(request.get('y'))
    ret = {}
    ret['result'] = do_add(x,y)

    response.set_body_json(ret)
    response.set_mime('json')
    return True
