import json

baseconfig = {
    "wifi": {
        "devicename": "Torwaechter",
        "ssid": "Torwaechter",
        "key": "12345678900",
        "apmode": "on"
        },
    "timer": {
          "wait_close": 60,
          "wait_alert": 60
        },
    "path": []
    }

CONFIG_FILE = 'sys.json'

def load_config(section=None):
    try:
        with open(CONFIG_FILE) as fp:
            data = json.load(fp)
            if section is not None:
                if section in data:
                    return data[section]
            return data

    except OSError:
        reset_config()
        return baseconfig

def save_config(config):
    with open(CONFIG_FILE, 'w') as fp:
        json.dump(config, fp)

def reset_config():
    save_config(baseconfig)
