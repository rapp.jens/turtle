# meine boot.py
# initialisiert die sd-karte und fügt diese als Suchpfad für Module hinzu
import uos
from usys import path
import network
from machine import Pin
from config import *

AP_PIN = 14 # Wemos D5

def create_ap(wifi):
    ssid = wifi['ssid']
    key = wifi['key']

    print('creating AP {}'.format(ssid))

    ap_if = network.WLAN(network.AP_IF) # create access-point interface
    ap_if.active(True)         # activate the interface
    ap_if.config(essid=ssid, password=key) # set the ESSID of the access point
    wlan = network.WLAN()
    wlan.ifconfig(('192.168.1.1','255.255.255.0','192.168.1.1','192.168.1.1'))


def connect_ap(wifi):
    ssid = wifi['ssid']
    key = wifi['key']

    sta_if = network.WLAN(network.STA_IF)

    # set device name
    if 'devicename' in wifi:
        sta_if.config(dhcp_hostname=wifi['devicename'])

    if not sta_if.isconnected():
        print('connecting network "{}"'.format(ssid))
        sta_if.active(True)
        sta_if.connect(ssid, key)
        while not sta_if.isconnected():
            pass
        print('Network configuration:', sta_if.ifconfig())


def connect_wifi(json):
    '''
    ' connect wifi if possible
    '''
    if 'wifi' in json:
        wifi = json['wifi']
        if wifi['apmode'] == "off":
            connect_ap(wifi)
            return

        create_ap(wifi)


def set_environment(json):
    global path
    '''
    ' set environment by json variables
    '''
    # additional paths
    if 'path' in json:
        for jpath in json['path']:
            print('appending {} to path'.format(jpath))
            path.append(jpath)

    # upip install path
    if 'pip_path' in json:
        ppath = json['pip_path']
        print('setting upip install path to {}'.format(ppath))
        path[1] = ppath


def test_pin_ap():
    '''
    Teste ob Pin D5 low ist. Wenn ja, aktiviere den Torwächter AP
    '''
    pin = Pin(AP_PIN, Pin.IN|Pin.PULL_UP)
    if pin.value() == 0:
        print("loading Baseconfig")
        wificonfig = baseconfig['wifi']
        create_ap(wificonfig)
        return True
    print("having normal config")
    return False


def print_network():
    wlan = network.WLAN()
    print("Network config")
    print(wlan.ifconfig())


def main_func():
    '''
    ' main
    '''
    json=load_config()

    if json is not None:
        set_environment(json)
        #if not test_pin_ap():
        connect_wifi(json)
        json = None

    print_network()


#run main
main_func()

