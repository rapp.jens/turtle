from turtlectl import Turtle

def handle_request(request, response):
    print ('running tmain')
    machinedata = request.machinedata

    print ('machinedata is of type {}'.format(type(machinedata)))

    ret = {}
    if isinstance(machinedata, Turtle):
        turtle = machinedata
        ret['result'] = turtle.set_animation(request.get('function'))
        ret['animations'] = list(turtle.animations.keys())
        print (ret)
    else:
        ret['error'] = 'no machine data set'

    response.set_body_json(ret)
    response.set_mime('json')
    return True

