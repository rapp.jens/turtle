'''
'turtle control
'
' test:
from turtlectl import Turtle
t = Turtle.instance()
t.set_animation('walk')
while True:
print(t.next_step, end=':')
t.run()


'''
from machine import Pin, PWM
import os
import json

class Turtle:
    '''
    representation for turtle class
    '''
    # the animation file to save
    CONFIG = 'animations.json'
    # a basic config to make it stand
    BASECONFIG = {
        'stand' : [[0,0,0,0]],
        'walk' : [
            [-.25,-.2,.2,.25],
            [.2,.25,-.25,-.2],
            ]
    }
    # singleton instance
    _instance = None

    def instance():
        '''
        we need a singleton
        '''
        if Turtle._instance is None:
            Turtle.instance = Turtle()
        return Turtle.instance

    def __init__(self):
        '''
        load config if possible,
        initialize legs
        set base animation
        '''
        self.load()
        self.init_legs()
        self.set_animation('stand')

    def load(self):
        '''
        load animations
        '''
        self.animations = Turtle.BASECONFIG
        if Turtle.CONFIG in os.listdir():
            with open(Turtle.CONFIG) as fp:
                self.animations = json.load(fp)

    def save(self):
        '''
        save animations
        '''
        with open(Turtle.CONFIG, 'w+') as fp:
            json.dump(self.animations, fp)

    def init_legs(self):
        '''
        initialize all legs
        '''
        self.legs = []
        self.legs.append(PWM(Pin(2),freq=50,duty=77))
        self.legs.append(PWM(Pin(0),freq=50,duty=77))
        self.legs.append(PWM(Pin(13),freq=50,duty=77))
        self.legs.append(PWM(Pin(12),freq=50,duty=77))


    def set_servo(self, number, pos):
        '''
        set duty value for single motor
        '''
        p = int(50 * pos + 77)
        self.legs[number].duty(p)


    def set_animation(self, name):
        '''
        set current_animation
        '''
        if name in self.animations:
            self.animation_name = name
            self.current_animation = self.animations[name]
            self.current_step = 0

        return self.animation_name

    def get_steps(self, name):
        '''
        get all steps for an animation
        '''
        if name in self.animations:
            return self.animations[name]
        return []

    def get_animation_step(self, step):
        '''
        get leg positions for current animation step
        '''
        return self.current_animation[step]

    def run(self):
        '''
        set next motor values
        '''
        maxsteps = len(self.current_animation)
        self.current_step = (self.current_step + 1) % maxsteps

        step = self.get_animation_step(self.current_step)

        for i in range(len(self.legs)):
            self.set_servo(i, step[i])

    def create_animation(self, name, values=[]):
        '''
        create a new animation
        set `values` if they are already known
        '''
        self.delete_animation(name)
        self.animations[name] = values
        return self.animations[name]

    def delete_animation(self, name):
        '''
        delete an animation with given `name`
        '''
        if name in self.animations:
            del self.animations[name]
