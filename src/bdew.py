from machine import Pin, PWM
import time

legs = []

animation_walk = [
        [-.25,-.2,.2,.25],
        [.2,.25,-.25,-.2],
        ]
def create_legs():
    legs.append(PWM(Pin(2),freq=50,duty=77))
    legs.append(PWM(Pin(0),freq=50,duty=77))
    legs.append(PWM(Pin(13),freq=50,duty=77))
    legs.append(PWM(Pin(12),freq=50,duty=77))


def set_servo(number, pos):
    p = int(50 * pos + 77)
    legs[number].duty(p)

def animate_servo(animation, step):
    step = step % len(animation)
    ast = animation[step]
    for i in range(len(legs)):
        print('running {}'.format(i))
        set_servo(i, ast[i])

def main():
    create_legs()
    
    step = 0
    while True:
        step += 1
        animate_servo(animation_walk, step)
        time.sleep(.5)


main()
