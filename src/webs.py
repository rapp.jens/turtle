# -*- coding: utf-8 -*-
# ##########################################################################
#
#  webs.py
# ##########################################################################
#
#  Autor   : tecdroid
#  Projekt : torwaechter - webserver
#  Stand   : 2021-02-05
#
#  Beschreibung: A simple micropython web server. useful for web based
#                control systems like
#
#
# Copyright (c) Jens Rapp <rapp.jens@gmail.com>
#
# The Regents of the University of California. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software must
#    display the following acknowledgement: “This product includes software
#    developed by the University of California, Berkeley and its contributors.”
# 4. Neither the name of the University nor the names of its contributors may be
#    used to endorse or promote products derived from this software without
#    specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS “AS IS” AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# ##########################################################################
import sys
import socket
import ssl
import json

'''
all mimes that are'nt listed here are interpreted as python script
it is also the mimes you can set for http response
'''
MIMES = {
    'js': 'text/javascript',
    'css': 'text/css',
    'png': 'image/png',
    'jpg': 'image/jpeg',
    'ico' : 'image/icon',
    'json': 'application/json',
    'html': 'text/html',
}

class Request:
    '''
    This contains any request data.
    Additionally, some machine data can be set. This is useful for stateful
    services
    '''
    # get and post methods
    GET = 'GET'
    # post data is not yet implemented
    POST = 'POST'
    def __init__(self, name, method=GET, parameters={}, machinedata=None):
        '''
        initialize using `name` as the called file, `method` as calling method,
        the request `parameters` as dictionary and `machinedata`, which is
        useful if you want to have some state which must be declared in your
        main program
        '''
        self.name = name
        self.method = method
        self.parameters = parameters
        self.machinedata = machinedata

    def get(self, key, default=None):
        '''
        get a parameter by `key` if it exists.
        otherwise, the value of `default` is returned
        '''
        if key in self.parameters:
            return self.parameters[key]
        return default

    def keys(self):
        '''
        list all parameter keys
        '''
        return self.parameters.keys()

    def __repr__(self):
        '''
        just to see whats inside for debugging
        '''
        ret = 'Request<{}> ['.format(self.name)
        for name, val in self.parameters:
            ret += '"{}" : "{}", '.format(name, val)
        ret += ']'
        return ret


class Response:
    '''
    this is where your services write their response
    '''
    def __init__(self, mime='html'):
        self.set_body('')
        self.set_mime(mime)

    def set_body(self, body):
        '''
        set response message body.
        `body` can contain html, json or anything else
        '''
        self.body = body

    def set_body_json(self, dictionary):
        '''
        if using services, you can directly put a `dictionary` here to generate
        a json type body
        '''
        self.set_mime('json')
        self.set_body(json.dumps(dictionary))

    def set_mime(self, mime):
        '''
        set the response mime
        '''
        self.mime = MIMES[mime]

    def __repr__(self):
        '''
        again, cool for debugging
        '''
        return 'Response: ({})'.format(self.body)


class Webserver:
    '''
    this is the web server itself
    '''
    def __init__(self, machinedata = None):
        '''
        initialize the socket server and start listening
        additionally, you can set `machinedata` which will be put into any
        request to keep your applications stateful
        '''

        self.machinedata = machinedata
        print('Machinedata of type {} set'.format(type(self.machinedata)))

        self.socket = socket.socket()
        ai = socket.getaddrinfo('0.0.0.0', 8080)

        print('Bind address info:', ai)
        addr = ai[0][-1]

        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(addr)

        self.socket.listen(5)

        print('Webs server up and running. h4v fun!')

    def run_step(self):
        '''
        run a step. this has to be set in your main loop
        '''
        res = self.socket.accept()
        client_s = res[0]
        client_addr = res[1]
        print('Client address:', client_addr)
        print('Client socket:', client_s)
        # client_s = ssl.wrap_socket(client_s, server_side=True)
        print(client_s)
        self.handle_client(client_s.makefile('rwb', 0))


    def create_response(self, response='200 OK', ctype='text/html'):
        '''
        creates an http resonse.
        `response` is the return code which is normally '200 OK'.
        `ctype` is the content-type of the response (see mimetype above)
        '''
        return "HTTP/1.1 {}\r\nContent-type: {}\r\n\r\n".format(response, ctype)

    def send_bytes(self, client, data):
        '''
        send byte `data` to the `client` socket
        '''
        client.write(data)


    def close(self, client):
        '''
        close a `client` socket
        '''
        print("done")
        self.send(client, '\r\n\r\n')
        client.close()

    def read(self, client):
        '''
        read data from `client` socket and return it
        '''
        val = client.readline().strip().decode("utf-8")
        print(val)
        return val

    def send(self, client, text):
        '''
        send `text` data to `client` socket
        '''
        print(text)
        self.send_bytes(client, bytes(text.encode('utf-8')))

    def send_404(self, client, filename):
        '''
        send 404 error to `client` with `filename` to know what's missing
        '''
        print("Datei {} nicht gefunden".format(filename))
        self.send(client, self.create_response('404 Not Found'))
        self.send(client, "Not found: " + filename)

    def split_param(self, params):
        '''
        deprecated
        split HTTP-GET - parameters
        `params` is the parameter string. A dictionary is returned
        '''
        plist = {}
        for param in params.split('&'):
            key, val = param.split('=')
            plist[key] = val
        return plist

    def extract_get_parameters(self, request):
        '''
        extract GET parameters from `request` object
        '''
        if '?' in request:
            fname, params = request.split('?')
            plist = {}
            for param in params.split('&'):
                key, val = param.split('=')
                print ("searching " +key)
                if key.find('[]') >= 0:
                    if key not in plist:
                        plist[key] = []
                    plist[key].append(val.replace('+',' '))
                else:
                    plist[key] = val.replace('+',' ')
            return fname, plist
        return request, {}

    def extract_header_parameters(self, request):
        '''
        POST parameters are not yet implemented
        '''
        print(request)
        #if '=' in request and ':' not in request:
        #    return self.split_param(request)
        return {}

    def test_file_exists(self, client, filename):
        '''
        test if file exists and send 404 if not
        `client` socket and `filename` is set
        '''
        # sende Datei wenn sie exitiert
        try:
            fp=open(filename)
            fp.close()
            return True
        except:
            # anderenfalls sende 404.
            self.send_404(client, filename)
            return False

    def serve_static(self, client, filename):
        '''
        serve a static file
        `client` socket and `filename` is set
        '''
        ext = filename.split('.')[-1]
        if ext not in MIMES.keys():
            return False

        if not self.test_file_exists(client, filename):
            return True

        with open(filename, 'rb') as fp:
            data = fp.read()
            resp=self.create_response(ctype=MIMES[ext])
            self.send(client, resp)
            self.send_bytes(client, data)

        return True

    def serve_active(self, client, fname, rtype, parameters):
        '''
        ' expect that `fname` is a python script.
        ' Ceates a Request object using `rtype` (GET/POST)
        ' and fills it with the given `parameters`
        ' After that, `fname` is imported and the function 'handle_request' is
        ' called.
        '
        ' # example request handler
        ' def handle_request(request, response):
              name = requeset.get('name', 'stranger')
              response_html = '<html><head><title>test</title></head><body>Hello {}</body></html>'.format(name)
        '     response.set_body(response_html)
        '
        ' the method deletes the called module after
        '''
        print ('serving active function {}'.format(fname))
        if self.test_file_exists(client, fname + '.py'):

            request = Request(
                    fname,
                    rtype,
                    parameters,
                    machinedata=self.machinedata
                    )

            response = Response()

            var = __import__(fname,  None, None, ['handle_request'])

            if var.handle_request(request, response) == True:
                self.send(client, self.create_response(ctype=response.mime))
                self.send(client, response.body)


            del(sys.modules[fname])

    def handle_client(self, client):
        '''
        ' handle client request
        '''
        try:
            # extract file and parameters from request
            headline = ''
            while headline == '':
                headline = self.read(client)
            print("Headline: " +headline)

            rtype, fstr, prot = headline.split(' ')
            fstr = fstr.replace('%5B%5D','[]').replace('%2C',',')
            print ("prepared fstr: " +fstr)
            fname, params = self.extract_get_parameters(fstr)

            print (params)

            # extract header data - post not yet implemented
            print (rtype, fname)
            if rtype == 'POST':
                print("post not yet implemented since i'm too stupid")
            while '' != self.read(client):
                pass

            # if no file name is given, use index.html
            if fname == '/':
                print ('replacing fname')
                fname = '/index.html'

            # fulfill the request
            print ('fname ' + fname)
            if not self.serve_static(client, fname[1:]):
                self.serve_active(client, fname[1:], rtype, params)

        except Exception as e:
            # any exception will be printed and replied
            print('Exception serving request:', e)
            try:
                self.send(client, self.create_response('505 Internal Server Error'))
                self.send(client, str(e))
            except:
                print('could not send to client.')
        self.close(client)
