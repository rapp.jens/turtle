function load_json(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4)
            if (this.status == 200) {
                //json = JSON.parse();
                callback(this.responseText);
           } else {
               callback( { "error" : this.readyState });
           }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

function serialize(form) {
    var ignore = ['file', 'reset', 'submit', 'button','datalist'];
    var field, s = [];
    if (typeof form == 'object' && form.nodeName == "FORM") {
        var len = form.elements.length;
        for (i=0; i<len; i++) {
            field = form.elements[i];
            console.log("checking field", field);
            if (field.name && !ignore.includes(field.type)) {
                console.log("adding field", field);
                if (field.type == 'select-multiple') {
                    for (j=form.elements[i].options.length-1; j>=0; j--) {
                        if(field.options[j].selected)
                            s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[j].value);
                    }
                } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
                    s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value);
                } else {
                    console.error("this should not happen");
                }
            }
        }
    }
    console.log(s);
    return form.action+'?'+s.join('&').replace(/%20/g, '+');
}

function send_form(formname, callback) {
    form = document.getElementById(formname);
    serialized = serialize(form);
    load_json(serialized, callback);
}

function prepare_form(form, callback) {
    form.addEventListener('submit', function(event) {
        event.preventDefault();
        fd = serialize(form);
        load_json(fd, callback);
    });
}

function do_hide(dialogname) {
    dialog = document.getElementById(dialogname);
    dialog.style.display="none";
}

function do_show(dialogname, mesg) {
    dialog = document.getElementById(dialogname);
    dialog.innerHTML=mesg;
    dialog.style.display="block";
}

