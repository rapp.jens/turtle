from turtlectl import Turtle
'''
Read animations from turtle
#todo: delete does not work
#todo: set animation after delete
'''
def create_action(request):
    '''
    create or save `animation`
    all data have to be in request
    '''
    turtle = request.machinedata
    # this is my action function
    if request.get('function') == 'create':

        animation = request.get('animation', 'unknown')
        steps = request.get('steps[]')
        steplist = []
        print("saving data " +  animation)
        print(steps)

        for step in steps:
            entries = []
            print("entries ", entries)
            for entry in step.split(','):
                entries.append(float(entry))
            steplist.append(entries)
            print("steplist ", steplist)

        turtle.create_animation(animation, steplist)
        turtle.save()


def delete_action(request):
    '''
    delete `animation`
    all data have to be in request
    '''
    turtle = request.machinedata
    # this is my action function
    if request.get('function') == 'delete':
        animation = request.get('animation', None)

        if animation is not None:
            turtle.delete_animation(animation)
            turtle.save()
            del request.parameters['animation']


def handle_request(request, response):
    '''
    createstep
    gives animation data back
    if parameter function is set, an animation will be saved
    '''
    turtle = request.machinedata

    ret = {}
    if isinstance(turtle, Turtle):

        # create animation if neccessary
        create_action(request)

        # delete animation if neccessary
        delete_action(request)

        # get animation to work on
        animation = request.get('animation', default=turtle.animation_name)

        # get steps
        steps = turtle.get_steps(animation)

        ret = {}
        ret['message'] = 'Saving done'
        ret['steps'] = steps
        ret['current'] = animation
        ret['animations'] = list(turtle.animations.keys())
        print (ret)

    else:
        ret['error'] = 'no machine data set'

    response.set_body_json(ret)
    response.set_mime('json')
    return True

