# -*- coding: utf-8 -*-
# ##########################################################################
#
#  main.py
# ##########################################################################
#
#  Autor   : tecdroid
#  Projekt : torwaechter - webserver
#  Stand   : 2021-02-05
#
#  Beschreibung:
#
# ##########################################################################
import sys
import socket
import json
from config import load_config
from webs import Webserver
from turtlectl import Turtle

from machine import Timer


def main(args):
    config = load_config('timer')

    turtle = Turtle.instance()
    server = Webserver(turtle)
    tim = Timer(-1)
    tim.init(period=500, mode=Timer.PERIODIC, callback=lambda t:turtle.run())

    while True:
        server.run_step()

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
