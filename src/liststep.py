from turtlectl import Turtle
'''
Read animations from turtle
'''
def handle_request(request, response):
    print ('running liststep')
    machinedata = request.machinedata

    ret = {}
    if isinstance(machinedata, Turtle):
        turtle = machinedata
        if 'action' in request.keys():
            ret['steps'] = turtle.get_steps(request.get('action'))
        ret['current'] = turtle.animation_name
        ret['animations'] = list(turtle.animations.keys())
        print (ret)
    else:
        ret['error'] = 'no machine data set'

    response.set_body_json(ret)
    response.set_mime('json')
    return True

